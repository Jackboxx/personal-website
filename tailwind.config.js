/** @type {import('tailwindcss').Config} */
export default {
    content: ["./src/**/*.{html,js,astro}"],
    theme: {
        fontFamily: {
            sansSerif: ["Open Sans"],
            sans: ["Varela Round"],
        },
        colors: {
            content: { dark: "#ffffff", light: "#000000" },
            subtle: { light: "#f6f5f4", dark: "#0a0e19" },
            primary: { light: "#565add", dark: "#565add" },
            secondary: { light: "#d1d1f7", dark: "#d1d1f7" },
            accent: { light: "#9f92ec", dark: "#9f92ec" },
        },
        extend: {},
    },
    plugins: [],
};
